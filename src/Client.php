<?php

namespace Milenium;

use Milenium\Crawler\CrawlerInterface;
use Milenium\Element\Article;
use Milenium\Element\Image;
use Milenium\Element\Site;
use Milenium\Exception\InvalidArgumentException;

/**
 * Class Client
 *
 * @package Milenium
 */
class Client
{

    /**
     * @var CrawlerInterface $crawler
     */
    private $crawler;

    /**
     * @var Site $data
     */
    private $data = [];

    /**
     * Client constructor.
     *
     * @param CrawlerInterface $crawler
     */
    public function __construct(CrawlerInterface $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * @param string $xmlString
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function fromString(string $xmlString): self
    {
        if (!$this->crawler->validateString($xmlString)) {
            throw new InvalidArgumentException();
        }
        $this->setData($this->crawler->fromString($xmlString));
        return $this;
    }

    /**
     * @param array $data
     * @throws InvalidArgumentException
     */
    private function setData(array $data): void
    {
        switch ($this->crawler->getType()) {
            case 'article':
                $class = Article::class;
                break;
            case 'site':
                $class = Site::class;
                break;
            case 'image':
                $class = Image::class;
                break;
            default:
                throw new InvalidArgumentException();
        }
        $this->data = new $class($data);
    }

    /**
     * @return CrawlerInterface
     */
    public function getCrawler(): CrawlerInterface
    {
        return $this->crawler;
    }

    /**
     * @return Site|Article|Image
     */
    public function getData()
    {
        return $this->data;
    }

}
