<?php

namespace Milenium\Crawler;

use DateTime;
use InvalidArgumentException;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class DomCrawler
 *
 * @package Milenium\Crawler
 */
class DomCrawler implements CrawlerInterface
{

    /**
     * Parse xml from xmlString
     *
     * @param string $xmlString
     *
     * @return array
     */
    public function fromString(string $xmlString): array
    {
        $crawledObject = new Crawler($xmlString);
        return $this->parseSite($crawledObject->filterXPath('//tpu'));
    }

    /**
     * Parse site
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    private function parseSite(Crawler $crawler)
    {
        return [
            'id' => (int)$crawler->attr('id'),
            'name' => $crawler->attr('name'),
            'sections' => $this->parseSections($crawler->filterXPath('//websection'))
        ];
    }

    /**
     * Parse sections
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    private function parseSections(Crawler $crawler)
    {
        return $crawler->each(function(Crawler $node) {
            return [
                'id' => (int)$node->attr('id'),
                'name' => $node->attr('name'),
                'articles' => $this->parseArticles($node->filterXPath('//item')),
            ];
        });
    }

    /**
     * Parse articles
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    private function parseArticles(Crawler $crawler)
    {
        return $crawler->each(function(Crawler $node) {
            return [
                'id' => (int)$node->attr('id'),
                'date' => DateTime::createFromFormat('d/m/Y H:i', $node->attr('from'))->format('c'),
                'xml' => $node->attr('path'),
            ];
        });
    }

    /**
     * Validate string is parseable
     *
     * @param string $xmlString
     *
     * @return bool
     */
    public function validateString(string $xmlString): bool
    {
        $crawledObject = new Crawler($xmlString);
        try {
            $crawledObject->filterXPath('//tpu')->nodeName();
            return true;
        } catch (InvalidArgumentException $exception) {
            return false;
        }
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string
    {
        return 'site';
    }

}
