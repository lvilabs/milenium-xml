<?php

namespace Milenium\Crawler;

use Closure;
use InvalidArgumentException;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ArticleDomCrawler
 *
 * @package Milenium\Crawler
 */
class ArticleDomCrawler implements CrawlerInterface
{

    /**
     * Parse xml from xmlString
     *
     * @param string $xmlString
     *
     * @return array
     */
    public function fromString(string $xmlString): array
    {
        $crawledObject = new Crawler($xmlString);
        return $this->parseArticle(
            $crawledObject->filterXPath('//MileniumXML/Article')
        );
    }

    /**
     * Parse Article
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseArticle(Crawler $crawler)
    {
        return [
            'id' => (int)$crawler->attr('id'),
            'headline' => $this->parseHeadline($crawler),
            'subheadline' => $this->parseSubheadline($crawler),
            'body' => $this->parseBody($crawler),
            'ribbon' => $this->parseRibbon($crawler),
            'image' => $this->parseImage($crawler)
        ];
    }

    /**
     * Parse headline
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseHeadline(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'TIT')]")
        );
    }

    /**
     * Parse subheadline
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseSubheadline(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'BAJ')]")
        );
    }

    /**
     * Parse body
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseBody(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'TXT')]")
        );
    }

    /**
     * Parse ribbon
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseRibbon(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'VOL')]")
        );
    }

    /**
     * Parse values
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseValues(Crawler $crawler)
    {
        return $crawler
            ->each(Closure::fromCallable([$this, 'parseParagraphs']));
    }

    /**
     * Parse paragraphs
     *
     * @param Crawler $values
     *
     * @return array
     */
    protected function parseParagraphs(Crawler $values)
    {
        return $values->filterXPath('//P')
            ->each(Closure::fromCallable([$this, 'processParagraph']));
    }

    /**
     * Process paragraph
     *
     * @param Crawler $paragraph
     *
     * @return array
     */
    protected function processParagraph(Crawler $paragraph)
    {
        $isTitle = preg_match(
            '/\bTIT|Subtitulo\b/',
            $paragraph->attr('stylename')
        );
        return [
            'type' => $isTitle ? 'header' : 'text',
            'content' => implode(" ", array_filter(
                $paragraph->filterXPath("//C")
                    ->each(Closure::fromCallable([$this, 'parseSentence']))
            ))
        ];
    }


    /**
     * Parse sentence
     *
     * @param Crawler $sentence
     *
     * @return string
     */
    protected function parseSentence(Crawler $sentence)
    {
        return trim(preg_replace('/\s+/', ' ', $sentence->html()));
    }

    protected function parseImage(Crawler $crawler)
    {
        $image = $crawler
            ->filterXPath("//Component[contains(@typename,'Image')]");

        if ($image->count() > 0) {
            return [
                'id' => (int)$image->attr('id'),
                'xml' => $image->attr('path')
            ];
        }

        return [];
    }

    /**
     * Validate string is parseable
     *
     * @param string $xmlString
     *
     * @return bool
     */
    public function validateString(string $xmlString): bool
    {
        $crawledObject = new Crawler($xmlString);
        try {
            $crawledObject->filterXPath('//MileniumXML/Article')->nodeName();
            return true;
        } catch (InvalidArgumentException $exception) {
            return false;
        }
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string
    {
        return 'article';
    }

}
