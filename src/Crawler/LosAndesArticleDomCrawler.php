<?php

namespace Milenium\Crawler;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class LosAndesArticleDomCrawler
 *
 * @package Milenium\Crawler
 */
class LosAndesArticleDomCrawler extends ArticleDomCrawler
{

    /**
     * Parse Article
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseArticle(Crawler $crawler)
    {
        return array_merge(
            parent::parseArticle($crawler),
            ['author' => $this->parseAuthor($crawler)]
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function parseSubheadline(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'BAJADA')]")
        );
    }

    /**
     * Parse author
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    private function parseAuthor(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'FIRMA')]")
        );
    }

    /**
     * {@inheritDoc}
     */
    protected function parseRibbon(Crawler $crawler)
    {
        return $this->parseValues(
            $crawler->filterXPath("//Component[contains(@name,'CINTILLO')]")
        );
    }

}
