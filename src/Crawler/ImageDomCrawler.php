<?php

namespace Milenium\Crawler;

use InvalidArgumentException;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ImageDomCrawler
 *
 * @package Milenium\Crawler
 */
class ImageDomCrawler implements CrawlerInterface
{

    /**
     * Parse xml from xmlString
     *
     * @param string $xmlString
     *
     * @return array
     */
    public function fromString(string $xmlString): array
    {
        $crawledObject = new Crawler($xmlString);
        return $this->parseImage(
            $crawledObject->filterXPath('//MileniumXML/Component')
        );
    }

    /**
     * Parse Image
     *
     * @param Crawler $crawler
     *
     * @return array
     */
    protected function parseImage(Crawler $crawler)
    {
        return [
            'url' => $crawler->attr('originalpath'),
        ];
    }

    /**
     * Validate string is parseable
     *
     * @param string $xmlString
     *
     * @return bool
     */
    public function validateString(string $xmlString): bool
    {
        $crawledObject = new Crawler($xmlString);
        try {
            $crawledObject->filterXPath('//MileniumXML/Component')->nodeName();
            return true;
        } catch (InvalidArgumentException $exception) {
            return false;
        }
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType(): string
    {
        return 'image';
    }

}
