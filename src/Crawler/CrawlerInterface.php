<?php

namespace Milenium\Crawler;

/**
 * Interface CrawlerInterface
 *
 * @package Milenium\Crawler
 */
interface CrawlerInterface
{

    /**
     * @param string $xmlString
     *
     * @return array
     */
    public function fromString(string $xmlString): array;

    /**
     * @param string $xmlString
     *
     * @return bool
     */
    public function validateString(string $xmlString): bool;

    /**
     * @return string
     */
    public function getType(): string;

}
