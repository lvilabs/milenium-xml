<?php


namespace Milenium\Element;


class Section
{

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var Article[] $sections
     */
    public $articles = [];

    /**
     * Section constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (!empty($data)) {
            if (!empty($data['id'])) {
                $this->id = $data['id'];
            }

            if (!empty($data['name'])) {
                $this->name = $data['name'];
            }

            if (!empty($data['articles'])) {
                foreach ($data['articles'] as $article) {
                    $this->articles[] = new Article($article);
                }
            }
        }
    }

}