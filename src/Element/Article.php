<?php

namespace Milenium\Element;

/**
 * Class Article
 *
 * @package Milenium\Element
 */
class Article
{

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $xml
     */
    public $xml;

    /**
     * @var string $date
     */
    public $date;

    /**
     * @var [][][] $headline
     */
    public $headline;

    /**
     * @var [][][] $subheadline
     */
    public $subheadline;

    /**
     * @var [][][] $body
     */
    public $body;

    /**
     * @var Image $image
     */
    public $image;

    /**
     * Article constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->updateProperties($data);
    }

    /**
     * Article update
     *
     * @param array $data
     */
    public function update($data = [])
    {
        $this->updateProperties($data);
    }

    /**
     * Update properties
     *
     * @param array $data
     */
    private function updateProperties($data = [])
    {
        if (!empty($data)) {
            if (!empty($data['id'])) {
                $this->id = $data['id'];
            }

            if (!empty($data['xml'])) {
                $this->xml = $data['xml'];
            }

            if (!empty($data['date'])) {
                $this->date = $data['date'];
            }

            if (!empty($data['headline'])) {
                $this->headline = $data['headline'];
            }

            if (!empty($data['subheadline'])) {
                $this->subheadline = $data['subheadline'];
            }

            if (!empty($data['body'])) {
                $this->body = $data['body'];
            }

            if (!empty($data['image'])) {
                $this->image = new Image($data['image']);
            }
        }
    }

}
