<?php

namespace Milenium\Element;

/**
 * Class Image
 *
 * @package Milenium\Element
 */
class Image
{

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $xml
     */
    public $xml;

    /**
     * @var string $url
     */
    public $url;

    /**
     * Image constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->updateProperties((array)$data);
    }

    /**
     * Image update
     *
     * @param array $data
     */
    public function update($data = [])
    {
        $this->updateProperties((array)$data);
    }

    /**
     * Update properties
     *
     * @param array $data
     */
    private function updateProperties($data = [])
    {
        if (!empty($data)) {
            if (!empty($data['id'])) {
                $this->id = $data['id'];
            }

            if (!empty($data['xml'])) {
                $this->xml = $data['xml'];
            }

            if (!empty($data['url'])) {
                $this->url = $data['url'];
            }
        }
    }

}
