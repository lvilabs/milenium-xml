<?php

namespace Milenium\Element;

/**
 * Class Site
 *
 * @package Milenium\Element
 */
class Site
{

    /**
     * @var integer $id
     */
    public $id;

    /**
     * @var string $name
     */
    public $name;

    /**
     * @var Section[] $sections
     */
    public $sections = [];

    /**
     * Site constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (!empty($data)) {
            if (!empty($data['id'])) {
                $this->id = (int)$data['id'];
            }

            if (!empty($data['name'])) {
                $this->name = $data['name'];
            }

            if (!empty($data['sections'])) {
                foreach ($data['sections'] as $section) {
                    $this->sections[] = new Section($section);
                }
            }
        }
    }

}
