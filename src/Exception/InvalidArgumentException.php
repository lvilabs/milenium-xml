<?php

namespace Milenium\Exception;

use Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Milenium\Exception
 */
class InvalidArgumentException extends Exception
{
}
