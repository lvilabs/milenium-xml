<?php

require_once __DIR__ . "/../vendor/autoload.php";

use Milenium\Client;
use Milenium\Crawler\ArticleDomCrawler;
use Milenium\Crawler\DomCrawler;
use Milenium\Exception\InvalidArgumentException as MileniumInvalidArgumentException;

// Site
$xmlString = file_get_contents(__DIR__ . "/../tests/stub/site.xml");
$client = new Client(new DomCrawler());
try {
    $test = $client->fromString($xmlString);
    var_dump(json_decode(json_encode($test->getData()), true));
} catch (MileniumInvalidArgumentException $e) {
    // Example
}

// Article
$xmlString = file_get_contents(__DIR__ . "/../tests/stub/article.xml");
$client = new Client(new ArticleDomCrawler());
try {
    $test = $client->fromString($xmlString);
    var_dump(json_decode(json_encode($test->getData()), true));
} catch (MileniumInvalidArgumentException $e) {
    // Example
}
