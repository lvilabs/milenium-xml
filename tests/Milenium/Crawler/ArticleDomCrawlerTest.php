<?php

namespace Test\Milenium\Crawler;

use Milenium\Crawler\ArticleDomCrawler;
use PHPUnit\Framework\TestCase;

class ArticleDomCrawlerTest extends TestCase
{

    public function testInvalidXmlString()
    {
        $this->assertFalse((new ArticleDomCrawler())->validateString("invalid string"));
    }

    public function testValidXmlString()
    {
        $xmlData = file_get_contents(__DIR__ . '../../../stub/article.xml');
        $crawler = new ArticleDomCrawler();
        $this->assertEquals('article', $crawler->getType());
        $this->assertTrue($crawler->validateString($xmlData));
        $parsedData = $crawler->fromString($xmlData);

        // Assert site details
        $this->assertEquals(1, $parsedData['id']);
        $this->assertEquals('<I>Lorem ipsum dolor sit amet, consectetur adipiscing posuere.</I>', $parsedData['headline'][0][0]['content']);
        $this->assertEquals('<I>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis sem est, ac lobortis orci vestibulum sed. Donec vel porttitor posuere. </I>', $parsedData['subheadline'][0][0]['content']);
        $this->assertEquals('A Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metus.', $parsedData['body'][0][0]['content']);
    }

}
