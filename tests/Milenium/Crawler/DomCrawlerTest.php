<?php

namespace Test\Milenium\Crawler;

use Milenium\Crawler\DomCrawler;
use PHPUnit\Framework\TestCase;

class DomCrawlerTest extends TestCase
{

    public function testInvalidXmlString()
    {
        $this->assertFalse((new DomCrawler())->validateString("invalid string"));
    }

    public function testValidXmlString()
    {
        $xmlData = file_get_contents(__DIR__ . '../../../stub/site.xml');
        $crawler = new DomCrawler();
        $this->assertEquals('site', $crawler->getType());
        $this->assertTrue($crawler->validateString($xmlData));
        $parsedData = $crawler->fromString($xmlData);

        // Assert site details
        $this->assertEquals(1, $parsedData['id']);
        $this->assertEquals('Test', $parsedData['name']);

        // Assert section details
        $this->assertEquals(2, count($parsedData['sections']));
        $this->assertEquals(1, $parsedData['sections'][0]['id']);
        $this->assertEquals('/$Test', $parsedData['sections'][0]['name']);
        $this->assertEquals(2, $parsedData['sections'][1]['id']);
        $this->assertEquals('/$Test.2', $parsedData['sections'][1]['name']);

        // Assert article details
        $this->assertEquals(14, count($parsedData['sections'][0]['articles']));
        $this->assertEquals(1, $parsedData['sections'][0]['articles'][0]['id']);
        $this->assertEquals('2019-11-18T18:52:00+00:00', $parsedData['sections'][0]['articles'][0]['date']);
        $this->assertEquals('Items\ART_1.xml', $parsedData['sections'][0]['articles'][0]['xml']);
        $this->assertEquals(10, $parsedData['sections'][0]['articles'][9]['id']);
        $this->assertEquals('2019-11-18T21:10:00+00:00', $parsedData['sections'][0]['articles'][9]['date']);
        $this->assertEquals('Items\ART_10.xml', $parsedData['sections'][0]['articles'][9]['xml']);
    }

}
