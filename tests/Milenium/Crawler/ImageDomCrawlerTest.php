<?php

namespace Test\Milenium\Crawler;

use Milenium\Crawler\ImageDomCrawler;
use PHPUnit\Framework\TestCase;

class ImageDomCrawlerTest extends TestCase
{

    public function testInvalidXmlString()
    {
        $this->assertFalse((new ImageDomCrawler())->validateString("invalid string"));
    }

    public function testValidXmlString()
    {
        $xmlData = file_get_contents(__DIR__ . '../../../stub/image.xml');
        $crawler = new ImageDomCrawler();
        $this->assertEquals('image', $crawler->getType());
        $this->assertTrue($crawler->validateString($xmlData));
        $parsedData = $crawler->fromString($xmlData);

        // Assert site details
        $this->assertEquals('data\image.jpg', $parsedData['url']);
    }

}
