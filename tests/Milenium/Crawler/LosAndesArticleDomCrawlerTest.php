<?php

namespace Test\Milenium\Crawler;

use Milenium\Crawler\LosAndesArticleDomCrawler;
use PHPUnit\Framework\TestCase;

class LosAndesArticleDomCrawlerTest extends TestCase
{

    public function testInvalidXmlString()
    {
        $this->assertFalse((new LosAndesArticleDomCrawler())->validateString("invalid string"));
    }

    public function testValidXmlString()
    {
        $xmlData = file_get_contents(__DIR__ . '../../../stub/article-los-andes.xml');
        $crawler = new LosAndesArticleDomCrawler();
        $this->assertEquals('article', $crawler->getType());
        $this->assertTrue($crawler->validateString($xmlData));
        $parsedData = $crawler->fromString($xmlData);

        // Assert site details
        $this->assertEquals(1, $parsedData['id']);
        $this->assertEquals('<I>Lorem ipsum dolor sit amet, consectetur adipiscing posuere.</I>', $parsedData['headline'][0][0]['content']);
        $this->assertEquals('<I>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis sem est, ac lobortis orci vestibulum sed. Donec vel porttitor posuere. </I>', $parsedData['subheadline'][0][0]['content']);
        $this->assertEquals('text', $parsedData['body'][0][0]['type']);
        $this->assertEquals('A Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi malesuada feugiat enim, aliquam maximus justo condimentum vel. In vulputate auctor nisl quis dapibus. Aenean metus.', $parsedData['body'][0][0]['content']);
        $this->assertEquals('header', $parsedData['body'][0][1]['type']);
        $this->assertEquals('Lorem malesuada', $parsedData['body'][0][1]['content']);
        $this->assertEquals('<I>John Doe</I>', $parsedData['author'][0][0]['content']);
        $this->assertEquals('john.doe@test.com', $parsedData['author'][0][1]['content']);
        $this->assertEquals('Lorem ipsum | Dolor sit arnet', $parsedData['ribbon'][0][0]['content']);
        $this->assertEquals(7, $parsedData['image']['id']);
        $this->assertEquals('image.xml', $parsedData['image']['xml']);
    }

}
