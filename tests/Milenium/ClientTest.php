<?php

namespace Test\Milenium;

use Milenium\Client;
use Milenium\Crawler\CrawlerInterface;
use Milenium\Element\Article;
use Milenium\Element\Site;
use Milenium\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{

    public function testCanCreateClient()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $client = new Client($crawler);
        $this->assertInstanceOf(Client::class, $client);
        $this->assertEquals($crawler, $client->getCrawler());
    }

    public function testCannotParseInvalidXmlString()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $this->expectException(InvalidArgumentException::class);
        $crawler->method('validateString')->willReturn(false);
        $client = new Client($crawler);
        $client->fromString('invalid xml');
    }

    public function testCanParseValidXmlString()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = [];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('site');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Site::class, $client->getData());
        $this->assertEquals($expected, array_filter((array)$client->getData()));
    }

    public function testCanParseSite()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = ['id' => 1, 'name' => 'Site'];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('site');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Site::class, $client->getData());
        $this->assertEquals($expected, array_filter((array)$client->getData()));
    }

    public function testCanParseSections()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = [
            'id' => 1,
            'name' => 'Site',
            'sections' => [
                ['id' => 1, 'name' => 'Section 1', 'articles' => []],
                ['id' => 2, 'name' => 'Section 2', 'articles' => []]
            ]
        ];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('site');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Site::class, $client->getData());
        $this->assertTrue(is_array($client->getData()->sections));
        $this->assertEquals(1, $client->getData()->sections[0]->id);
        $this->assertEquals('Section 1', $client->getData()->sections[0]->name);
        $this->assertEquals($expected, array_filter(json_decode(json_encode($client->getData()), true)));
    }

    public function testCanParseArticles()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = [
            'id' => 1,
            'name' => 'Site',
            'sections' => [
                [
                    'id' => 1,
                    'name' => 'Section',
                    'articles' => [
                        [
                            'id' => 1,
                            'xml' => 'xml_1.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['test headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 1,
                                'xml' => 'image_1.xml',
                                'url' => 'image-2.jpg',
                            ]
                        ],
                        [
                            'id' => 2,
                            'xml' => 'xml_2.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['test headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 2,
                                'xml' => 'image_2.xml',
                                'url' => 'image-2.jpg',
                            ]
                        ],
                    ],
                ],
            ],
        ];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('site');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Site::class, $client->getData());
        $this->assertTrue(is_array($client->getData()->sections[0]->articles));
        $this->assertEquals(2, count($client->getData()->sections[0]->articles));
        $this->assertEquals(1, $client->getData()->sections[0]->articles[0]->id);
        $this->assertEquals($expected, array_filter(json_decode(json_encode($client->getData()), true)));
    }

    public function testSetDataArticle()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = [
            'id' => 1,
            'name' => 'Article',
        ];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('article');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Article::class, $client->getData());
        $this->assertEquals(1, $client->getData()->id);
    }

    public function testSetDataError()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $this->expectException(InvalidArgumentException::class);
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('getType')->willReturn('invalid type');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $client->getData();
    }

    public function testCanUpdateArticle()
    {
        $crawler = $this->getMockBuilder(CrawlerInterface::class)->getMock();
        $expected = [
            'id' => 1,
            'name' => 'Site',
            'sections' => [
                [
                    'id' => 1,
                    'name' => 'Section',
                    'articles' => [
                        [
                            'id' => 1,
                            'xml' => 'xml_1.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['test headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 1,
                                'xml' => 'image_1.xml',
                                'url' => 'image-1.jpg',
                            ]
                        ],
                        [
                            'id' => 2,
                            'xml' => 'xml_2.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['test headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 2,
                                'xml' => 'image_2.xml',
                                'url' => 'image-2.jpg',
                            ]
                        ],
                    ],
                ],
            ],
        ];

        $updated = [
            'id' => 1,
            'name' => 'Site',
            'sections' => [
                [
                    'id' => 1,
                    'name' => 'Section',
                    'articles' => [
                        [
                            'id' => 1,
                            'xml' => 'xml_1.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['updated headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 1,
                                'xml' => 'image_1.xml',
                                'url' => 'image-1.jpg',
                            ]
                        ],
                        [
                            'id' => 2,
                            'xml' => 'xml_2.xml',
                            'date' => '1970-01-01T00:00:00+3000',
                            'headline' => [['test headline']],
                            'subheadline' => [['test subheadline']],
                            'body' => [['test body']],
                            'image' => [
                                'id' => 2,
                                'xml' => 'image_2.xml',
                                'url' => 'image-2.jpg',
                            ]
                        ],
                    ],
                ],
            ],
        ];
        $crawler->method('validateString')->willReturn(true);
        $crawler->method('fromString')->willReturn($expected);
        $crawler->method('getType')->willReturn('site');
        $client = new Client($crawler);
        $client->fromString('valid xml');
        $this->assertInstanceOf(Site::class, $client->getData());
        $this->assertTrue(is_array($client->getData()->sections[0]->articles));
        $client->getData()->sections[0]->articles[0]
            ->update(['headline' => [['updated headline']]]);
        $this->assertEquals($updated, json_decode(json_encode($client->getData()), true));
    }

}
